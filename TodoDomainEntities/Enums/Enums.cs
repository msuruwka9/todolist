﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TodoDomainEntities.Enums
{
        /// <summary>
        /// Types of list status.
        /// </summary>
        public enum ListStatus
        {
            /// <summary>
            /// List can be ssen
            /// </summary>
            Show,

            /// <summary>
            /// List is hidden
            /// </summary>
            Hide,

            /// <summary>
            /// List need to be delete
            /// </summary>
            Delete,
        }

        /// <summary>
        /// Statuses of entries.
        /// </summary>
        public enum EntryStatus
        {
		/// <summary>
		/// Entry is completed
		/// </summary>
	        [Display(Name = "Completed")]
		    Completed,

        /// <summary>
        /// Entry is in progess
        /// </summary>
            [Display(Name = "In progress")]
            InProgress,

		/// <summary>
		/// Entry is not started
		/// </summary>
		    [Display(Name = "Not started")]
		    NotStarted,
	    }
}
