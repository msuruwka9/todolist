﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TodoDomainEntities.Infrastructure
{
	[AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
	public sealed class IsValidDueDateAttribute : ValidationAttribute
	{
		public override bool IsValid(object? value)
		{
			if (value is null)
			{
				return false;
			}

			var dueDate = (DateTime)value;
			var actualDate = DateTime.Now.Date;

			if (DateTime.Compare(dueDate, actualDate) < 0)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
	}
}
