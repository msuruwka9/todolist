﻿using Microsoft.EntityFrameworkCore;
using TodoDomainEntities.Enums;

namespace TodoDomainEntities.Models
{
    public static class SeedData
    {
        public static void Initialize(TodoAppDbContext context)
        {
            if (context.Database.GetPendingMigrations().Any())
            {
                context.Database.Migrate();
            }

            if (!context.Entries.Any())
            {
				var todoLists = new List<TodoList>
				{
					new TodoList
					{
						Title = "Home tasks",
						Status = ListStatus.Show,
						Entries = new List<Entry>
						{
							new Entry
							{
								Title = "Wash the dishes",
								Description = "Don't forget to wash the dishes",
								Status = EntryStatus.InProgress,
								DueDate = DateTime.Now.AddDays(11),
							},
                            new Entry
							{
								Title = "Clean the bathroom",
								Description = "Clean the bathroom sink and shower",
								Status = EntryStatus.NotStarted,
								DueDate = DateTime.Now.AddDays(1),
							},
                        },
                    },
                    new TodoList
					{
						Title = "Work tasks",
						Status = ListStatus.Show,
						Entries = new List<Entry>
						{
							new Entry
							{
								Title = "Prepare for meeting",
								Description = "Prepare presentation for the meeting",
								Status = EntryStatus.Completed,
								DueDate = DateTime.Now.AddDays(5),
							},
							new Entry
							{
								Title = "Submit report",
								Description = "Submit report on sales figures",
								Status = EntryStatus.InProgress,
								DueDate = DateTime.Now.AddDays(21),
							},
						},
					},
                    new TodoList
					{
						Title = "Vacation planning",
						Status = ListStatus.Hide,
						Entries = new List<Entry>
						{
							new Entry
							{
								Title = "Book hotel",
								Description = "Book a hotel for the trip",
								Status = EntryStatus.Completed,
								DueDate = DateTime.Now.AddDays(11),
							},
							new Entry
							{
								Title = "Make travel arrangements",
								Description = "Book flights and rental car",
								Status = EntryStatus.NotStarted,
								DueDate = DateTime.Now.AddDays(7),
							},
						},
					},
                    new TodoList
					{
						Title = "Grocery shopping",
						Status = ListStatus.Show,
						Entries = new List<Entry>
						{
							new Entry
							{
								Title = "Buy milk",
								Description = "Get a gallon of whole milk",
								Status = EntryStatus.Completed,
								DueDate = DateTime.Now.AddDays(19),
							},
							new Entry
							{
								Title = "Purchase vegetables",
								Description = "Buy lettuce, tomatoes, and carrots",
								Status = EntryStatus.Completed,
								DueDate = DateTime.Now.AddDays(11),
							},
						},
					},
                    new TodoList
					{
						Title = "Weekend plans",
						Status = ListStatus.Hide,
						Entries = new List<Entry>
						{
							new Entry
							{
								Title = "Go to the movies",
								Description = "See the latest action movie at the theater",
								Status = EntryStatus.InProgress,
								DueDate = DateTime.Now.AddDays(10),
							},
							new Entry
							{
								Title = "Have dinner at restaurant",
								Description = "Go to Italian restaurant for dinner",
								Status = EntryStatus.Completed,
								DueDate = DateTime.Now.AddDays(6),
							},
						},
					},
                };

                context.TodoList.AddRange(todoLists);
                context.SaveChanges();
			}
        }
    }
}
