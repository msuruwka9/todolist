﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace TodoDomainEntities.Models.Repository
{
    public class TodoListRepository : ITodoListRepository
    {
        private readonly TodoAppDbContext context;

        public TodoListRepository(TodoAppDbContext ctx)
        {
            this.context = ctx;
        }

		public IQueryable<TodoList> TodoLists => this.context.TodoList;

		public IQueryable<Entry> Entries => this.context.Entries;

		public void CreateEntry(Entry entry, int todoListId)
		{
			TodoList? tdl = this.context.TodoList.FirstOrDefault(td => td.Id == todoListId);

			if (tdl is not null)
			{
				this.context.Add(entry);
				tdl.Entries.Add(entry);
			}

			this.context.SaveChanges();
		}

		public void CreateTodoList(TodoList tdl)
		{
			this.context.Add(tdl);
			this.context.SaveChanges();
		}

		public void DeleteEntry(Entry entry)
		{
			this.context.Remove(entry);
			this.context.SaveChanges();
		}

		public void DeleteTodoList(TodoList tdl)
		{
			this.context.Remove(tdl);
			this.context.SaveChanges();
		}

		public void EditEntry(Entry entry)
		{
				Entry? entryToEdit = this.context.Entries.FirstOrDefault(e => e.Id == entry.Id);
				if (entryToEdit is not null)
				{
					entryToEdit.Status = entry.Status;
					entryToEdit.Title = entry.Title;
					entryToEdit.Description = entry.Description;
					entryToEdit.DueDate = entry.DueDate;
				}

			this.context.SaveChanges();
		}

		public void EditTodoList(TodoList tdl)
		{
			if (tdl.Id == 0)
			{
				this.context.Add(tdl);
			}
			else
			{
				TodoList? tdlEntry = this.context.TodoList.FirstOrDefault(td => td.Id == tdl.Id);

				if (tdlEntry is not null)
				{
					tdlEntry.Title = tdl.Title;
					tdlEntry.Status = tdl.Status;
				}
			}

			this.context.SaveChanges();
		}

		public void CopyTodoList(long todoListId)
		{
			TodoList? originalList = this.context.TodoList.Include("Entries").AsNoTracking().FirstOrDefault(tdl => tdl.Id == todoListId);

			if (originalList is not null)
			{
				originalList.Id = 0;

				for (int i = 0; i < originalList.Entries.Count; i++)
				{
					originalList.Entries[i].Id = 0;
				}

				this.context.Add(originalList);
			}

			this.context.SaveChanges();
		}
	}
}
