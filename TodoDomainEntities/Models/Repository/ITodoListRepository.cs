﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TodoDomainEntities.Models.Repository
{
    public interface ITodoListRepository
    {
        IQueryable<TodoList> TodoLists { get; }

        IQueryable<Entry> Entries { get; }

        void EditTodoList(TodoList tdl);

        void CreateTodoList(TodoList tdl);

        void DeleteTodoList(TodoList tdl);

        void EditEntry(Entry entry);

        void CreateEntry(Entry entry, int todoListId);

        void DeleteEntry(Entry entry);

		void CopyTodoList(long todoListId);
	}
}
