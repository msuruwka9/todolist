﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoDomainEntities.Enums;
using TodoDomainEntities.Infrastructure;

namespace TodoDomainEntities.Models
{
    public class Entry
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter a title")]
        [StringLength(40, MinimumLength = 4, ErrorMessage = "Title must be shorter than 40 letter and longer than 4 letters")]
        public string Title { get; set; } = string.Empty;

        [Required(ErrorMessage = "Please enter a description")]
        [MaxLength(100, ErrorMessage = "Description must be shorter than 100 letters")]
        public string Description { get; set; } = string.Empty;

        [Required(ErrorMessage = "Please select the status")]
        public EntryStatus Status { get; set; }

        [DataType(DataType.Date)]
		[Required(ErrorMessage = "Please enter a due date")]
		[IsValidDueDate(ErrorMessage = "Due date cannot be earlier than today")]
        public DateTime DueDate { get; set; }

        public DateTime CreationTime { get; set; }

		[MaxLength(450)]
		public string? UserId { get; set; } 

        public TodoList? TodoList { get; set; }

        public int TodoListId { get; set; }
    }
}
