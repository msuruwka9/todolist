﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoDomainEntities.Enums;

namespace TodoDomainEntities.Models
{
    public class TodoList
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter a title")]
        [StringLength(40, MinimumLength = 4, ErrorMessage = "Title must be shorter than 40 letter and longer than 4 letters")]
        public string Title { get; set; } = string.Empty;

        [Required(ErrorMessage = "Please select the status")]
        public ListStatus Status { get; set; }

        [MaxLength(450)]
        public string? UserId { get; set; } 

        public List<Entry> Entries { get; set; } = new List<Entry>();
    }
}
