﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TodoDomainEntities.Migrations
{
    /// <inheritdoc />
    public partial class EntryDefaultValue : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
        }

		/// <inheritdoc />
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Critical Code Smell", "S1186:Methods should not be empty", Justification = "Empty Migration")]
        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}
