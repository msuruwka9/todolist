﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TodoDomainEntities.Migrations
{
    /// <inheritdoc />
    public partial class DeleteDefaultValueEntry : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Status",
                table: "Entries",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldDefaultValue: "Uncompleted");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Status",
                table: "Entries",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "Uncompleted",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");
        }
    }
}
