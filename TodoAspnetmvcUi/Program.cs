using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using TodoDomainEntities.Models;
using TodoDomainEntities.Models.Repository;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllersWithViews();

var connectionString = builder.Configuration["ConnectionStrings:TodoListConnection"];

if (!string.IsNullOrEmpty(connectionString))
{
	builder.Services.AddDbContext<TodoAppDbContext>(opts =>
	{
		opts.UseSqlServer(connectionString);
	});
}

var connectionStringIdentity = builder.Configuration["ConnectionStrings:IdentityConnection"];

if (!string.IsNullOrEmpty(connectionStringIdentity))
{
	builder.Services.AddDbContext<AppIdentityDbContext>(options
	=> options.UseSqlServer(connectionStringIdentity));
}

builder.Services.AddDefaultIdentity<IdentityUser>(options =>
	{
		options.SignIn.RequireConfirmedAccount = true;
		options.Password.RequireDigit = false;
		options.Password.RequiredLength = 6;
		options.Password.RequireNonAlphanumeric = false;
		options.Password.RequireUppercase = false;
		options.Password.RequireLowercase = false;
	}).AddEntityFrameworkStores<AppIdentityDbContext>();

builder.Services.AddScoped<ITodoListRepository, TodoListRepository>();

builder.Services.AddDistributedMemoryCache();

var app = builder.Build();

if (app.Environment.IsProduction())
{
	app.UseExceptionHandler("/Error");
}

app.UseStaticFiles();

app.MapDefaultControllerRoute();

app.UseAuthentication();

app.UseAuthorization();

using (var scope = app.Services.CreateScope())
{
	var services = scope.ServiceProvider;
	try
	{
		var context = services.GetRequiredService<TodoAppDbContext>();
		SeedData.Initialize(context);
	}
	catch (Exception)
	{
		Console.WriteLine("An error occurred while seeding the database.");
	}
}

app.MapControllerRoute(
	name: "error",
	pattern: "Error",
	defaults: new { Controller = "Home", action = "Error" });

app.MapRazorPages();

app.Run();
