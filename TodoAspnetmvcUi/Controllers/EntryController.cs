﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TodoDomainEntities.Enums;
using TodoDomainEntities.Models;
using TodoDomainEntities.Models.Repository;

namespace TodoAspnetmvcUi.Controllers
{
    [Authorize]
    [Route("Entry")]
    public class EntryController : Controller
    {
        private readonly ITodoListRepository repository;
        private readonly UserManager<IdentityUser> userManager;

        public EntryController(ITodoListRepository repository, UserManager<IdentityUser> userManager)
        {
            this.repository = repository;
            this.userManager = userManager;
		}

        [Route("Details/{entryId:long}")]
        public ViewResult Details(long entryId) 
        {
            var entry = this.repository.Entries
                .FirstOrDefault(x => x.Id == entryId && x.UserId == this.userManager.GetUserId(this.User));

            return this.View(entry);
        }

        [Route("Create")]
        public ViewResult Create(int todolistId)
        {
            var entry = new Entry { TodoListId = todolistId };

            return this.View(entry);
        }

        [HttpPost]
        [Route("Create")]
        public IActionResult Create(int todolistId, Entry entry)
        {
            entry.TodoListId = todolistId;
            entry.UserId = this.userManager.GetUserId(this.User);

            if (this.ModelState.IsValid)
            {
                this.repository.CreateEntry(entry, todolistId);
                return this.RedirectToAction("Index", "Home");
            }

            return this.View(entry);
        }

        [HttpGet]
        [Route("Edit/{entryId:long}")]
        public ViewResult Edit(long entryId)
        {
            return this.View(this.repository.Entries.FirstOrDefault(e => e.Id == entryId && e.UserId == this.userManager.GetUserId(this.User)));
        }

        [HttpPost]
        [Route("Edit/{entryId:long}")]
        public IActionResult Edit(int todolistId, Entry entry)
        {
            if (entry.UserId != this.userManager.GetUserId(this.User))
            {
                return this.RedirectToAction("Index", "Home");
            }

            if (this.ModelState.IsValid)
            {
                this.repository.EditEntry(entry);
                return this.RedirectToAction("Details", "Home", new { todolistId = todolistId });
            }

            return this.View(entry);
        }

        [HttpGet]
        [Route("Delete/{entryId:long}")]
        public ViewResult Delete(long entryId)
            => this.View(this.repository.Entries.FirstOrDefault(e => e.Id == entryId && e.UserId == this.userManager.GetUserId(this.User)));

        [HttpPost]
        [Route("Delete/{entryId:long}")]
        public IActionResult DeleteEntry(long entryId)
        {
            var entry = this.repository.Entries.FirstOrDefault(e => e.Id == entryId && e.UserId == this.userManager.GetUserId(this.User));
            if (entry is null)
            {
                return this.RedirectToAction("Index", "Home");
            }

            this.repository.DeleteEntry(entry);
            return this.RedirectToAction("Index", "Home");
        }

        [Route("Completed/{todoListId:long}")]
        public ViewResult ShowCompleted(long todolistId)
        {
            var entries = this.repository.Entries
                .Where(e => e.TodoListId == todolistId && e.Status == EntryStatus.Completed && e.UserId == this.userManager.GetUserId(this.User));

            return this.View(entries);
		}

        [Route("DueToday")]
        public ViewResult ShowDueToday()
        {
            DateTime actualDate = DateTime.Now.Date;

            var entries = this.repository.Entries.Where(e => DateTime.Compare(actualDate, e.DueDate.Date) == 0 && e.UserId == this.userManager.GetUserId(this.User));

            return this.View(entries);
        }
	}
}
