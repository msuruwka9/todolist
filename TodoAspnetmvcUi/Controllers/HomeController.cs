﻿using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TodoDomainEntities.Enums;
using TodoDomainEntities.Models;
using TodoDomainEntities.Models.Repository;

namespace TodoAspnetmvcUi.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ITodoListRepository repository;
        private readonly UserManager<IdentityUser> userManager;

        public HomeController(ITodoListRepository repository, UserManager<IdentityUser> userManager)
        {
            this.repository = repository;
            this.userManager = userManager;
        }

        public IActionResult Index() 
            => this.View(this.repository.TodoLists
                .Include(tdl => tdl.Entries.Where(e => e.Status != EntryStatus.Completed))
                .Where(tdl => tdl.Status == ListStatus.Show && tdl.UserId == this.userManager.GetUserId(this.User)));

        [Route("Details/{todolistId:long}")]
        public ViewResult Details(long todolistId)
        {
            return this.View(this.repository.TodoLists
                .Include(tdl => tdl.Entries.Where(e => e.Status != EntryStatus.Completed))
                .FirstOrDefault(tdl => tdl.Id == todolistId && tdl.UserId == this.userManager.GetUserId(this.User)));
        }

        [Route("TodoList/Edit/{todolistId:long}")]
        public ViewResult Edit(int todolistId)
        {
            return this.View(this.repository.TodoLists.FirstOrDefault(tdl => tdl.Id == todolistId && tdl.UserId == this.userManager.GetUserId(this.User)));
        }

        [HttpPost]
        [Route("TodoList/Edit/{todolistId:long}")]
        public IActionResult Edit(TodoList todolist)
        {
            if (todolist.UserId != this.userManager.GetUserId(this.User))
            {
                return this.RedirectToAction("Index");
            }

            if (this.ModelState.IsValid)
            {
                this.repository.EditTodoList(todolist);
                return this.RedirectToAction("Index");
            }

            return this.View(todolist);
        }

        [Route("TodoList/Create")]
        public ViewResult Create()
        {
            return this.View(new TodoList());
        }

        [HttpPost]
        [Route("TodoList/Create")]
        public IActionResult Create(TodoList todolist)
        {
            if (this.ModelState.IsValid) 
            {
                var userId = this.userManager.GetUserId(this.User);
                todolist.UserId = userId;

                this.repository.CreateTodoList(todolist);
                return this.RedirectToAction("Index");
            }

            return this.View(todolist);
        }

        [Route("TodoList/Delete/{todolistId:long}")]
        public IActionResult Delete(long todolistId)
            => this.View(this.repository.TodoLists.FirstOrDefault(tdl => tdl.Id == todolistId && tdl.UserId == this.userManager.GetUserId(this.User)));

        [HttpPost]
        [Route("TodoList/Delete/{todolistId:long}")]
        public IActionResult DeleteTodoList(long todolistId)
        {
            var todoList = this.repository.TodoLists.Include(tdl => tdl.Entries).FirstOrDefault(tdl => tdl.Id == todolistId);
            if (todoList == null || todoList.UserId != this.userManager.GetUserId(this.User))
            {
                return this.RedirectToAction("Index");
            }

            this.repository.DeleteTodoList(todoList);
            return this.RedirectToAction("Index");
        }

        [Route("TodoList/Hidden")]
        public ViewResult ShowHidden()
		    => this.View(this.repository.TodoLists
                .Include(tdl => tdl.Entries.Where(e => e.Status != EntryStatus.Completed))
                .Where(tdl => tdl.Status == ListStatus.Hide && tdl.UserId == this.userManager.GetUserId(this.User)));

        [Route("TodoList/Copy")]
        public IActionResult CopyList(long todolistId)
        {
            var todoList = this.repository.TodoLists.FirstOrDefault(tdl => tdl.Id == todolistId && tdl.UserId == this.userManager.GetUserId(this.User));
            
            if (todoList is not null)
            {
				this.repository.CopyTodoList(todolistId);
			}

            return this.RedirectToAction("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
		{
			return this.View();
		}

	}
}
