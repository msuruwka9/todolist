﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TodoDomainEntities.Models;
using TodoDomainEntities.Models.Repository;

namespace TodoAspnetmvcUi.Components
{
	public class EntriesListViewComponent : ViewComponent
	{
		private readonly ITodoListRepository repository;

		public EntriesListViewComponent(ITodoListRepository repository)
		{
			this.repository = repository;
		}

		public IViewComponentResult Invoke(Entry entry)
		{
			return this.View(entry);
		}
	}
}
