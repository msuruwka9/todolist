using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using NUnit.Framework.Internal;
using System;
using TodoDomainEntities.Enums;
using TodoDomainEntities.Models;
using TodoDomainEntities.Models.Repository;

namespace TodoAppTests
{
    public class TodoDomainEntitiesTests
    {
        private static DbContextOptions<TodoAppDbContext> appDbContextOptions = new DbContextOptionsBuilder<TodoAppDbContext>()
            .UseInMemoryDatabase(databaseName: "testdatabase")
            .Options;

        private TodoAppDbContext appDbContext;
        private ITodoListRepository repository;

        [SetUp]
        public void SetUp()
        {
            this.appDbContext = new TodoAppDbContext(appDbContextOptions);
            this.appDbContext.Database.EnsureCreated();
            this.repository = new TodoListRepository(this.appDbContext);
        }

        [Test]
        public void CreateEntry_AddsEntryToTodoList()
        {
            // Arrange
            var todoList = new TodoList { Id = 1 };
            this.appDbContext.TodoList.Add(todoList);
            this.appDbContext.SaveChanges();

            var entry = new Entry { Id = 1, Title = "Test Entry" };
            var todoListId = 1;

            // Act
            this.repository.CreateEntry(entry, todoListId);

            // Assert
            var todoListAfter = this.appDbContext.TodoList
                .Include(td => td.Entries)
                .FirstOrDefault(td => td.Id == todoListId);

            Assert.That(todoListAfter, Is.Not.Null);
            Assert.That(todoListAfter.Entries, Has.Member(entry));
        }

        [Test]
        public void CreateEntry_WithNonExistingTodoListId_DoesNotAddEntry()
        {
            // Arrange
            var entry = new Entry { Title = "Test Entry", Status = EntryStatus.NotStarted };
            var todoListId = 99;

            // Act
            this.repository.CreateEntry(entry, todoListId);

            // Assert
            var createdEntry = this.appDbContext.Entries.FirstOrDefault(e => e.Title == "Test Entry");
            var todoList = this.appDbContext.TodoList.FirstOrDefault(td => td.Id == todoListId);

            Assert.That(createdEntry, Is.Null);
            Assert.That(todoList, Is.Null);
        }

        [Test]
        public void EditEntry_UpdatesExistingEntry()
        {
            // Arrange
            var entry = new Entry { Id = 1, Title = "Test Entry", Status = EntryStatus.InProgress };
            this.appDbContext.Entries.Add(entry);
            this.appDbContext.SaveChanges();

            // Act
            var updatedEntry = new Entry { Id = 1, Title = "Updated Entry", Status = EntryStatus.NotStarted };
            this.repository.EditEntry(updatedEntry);

            // Assert
            var editedEntry = this.appDbContext.Entries.FirstOrDefault(e => e.Id == updatedEntry.Id);

            Assert.That(editedEntry, Is.Not.Null);
            Assert.That(editedEntry.Title, Is.EqualTo(updatedEntry.Title));
            Assert.That(editedEntry.Status, Is.EqualTo(updatedEntry.Status));
        }

        [Test]
        public void EditEntry_WithNonExistingEntryId_DoesNotUpdateEntry()
        {
            // Arrange
            var entry = new Entry { Id = 1, Title = "Test Entry", Status = EntryStatus.NotStarted };

            // Act
            this.repository.EditEntry(entry);

            // Assert
            var editedEntry = this.appDbContext.Entries.FirstOrDefault(e => e.Id == 1);

            Assert.That(editedEntry, Is.Null);
        }

        [Test]
        public void EditTodoList_UpdatesExistingTodoList()
        {
            // Arrange
            var todoList = new TodoList { Id = 1, Title = "Test TodoList", Status = ListStatus.Hide };
            this.appDbContext.TodoList.Add(todoList);
            this.appDbContext.SaveChanges();

            // Act
            var updatedTodoList = new TodoList { Id = 1, Title = "Updated TodoList", Status = ListStatus.Show };
            this.repository.EditTodoList(updatedTodoList);

            // Assert
            var editedTodoList = this.appDbContext.TodoList.FirstOrDefault(td => td.Id == updatedTodoList.Id);

            Assert.That(editedTodoList, Is.Not.Null);
            Assert.That(editedTodoList.Title, Is.EqualTo(updatedTodoList.Title));
            Assert.That(editedTodoList.Status, Is.EqualTo(updatedTodoList.Status));
        }

        [Test]
        public void EditTodoList_TodoListIdIsNegative()
        {
            // Arrange
            var todoList = new TodoList { Id = -1, Title = "Test Todo List", Status = ListStatus.Show };

            // Act
            this.repository.EditTodoList(todoList);

            // Assert
            var editedTodoList = this.appDbContext.TodoList.FirstOrDefault(tdl => tdl.Id == todoList.Id);

            Assert.That(editedTodoList, Is.Null);
        }


        [Test]
        public void CopyTodoList_CreatesNewTodoListWithSameEntries()
        {
            // Arrange
            var todoList = new TodoList { Id = 1, Title = "Test TodoList", Status = ListStatus.Show };
            todoList.Entries.Add(new Entry { Id = 1, Title = "Test Entry 1", Status = EntryStatus.NotStarted });
            todoList.Entries.Add(new Entry { Id = 2, Title = "Test Entry 2", Status = EntryStatus.InProgress });
            this.appDbContext.TodoList.Add(todoList);
            this.appDbContext.SaveChanges();

            // Act
            this.repository.CopyTodoList(1);

            // Assert
            var copiedTodoList = this.appDbContext.TodoList.Include(tdl => tdl.Entries).FirstOrDefault(tdl => tdl.Id != 1);

            Assert.That(copiedTodoList, Is.Not.Null);
            Assert.That(copiedTodoList.Title, Is.EqualTo(todoList.Title));
            Assert.That(copiedTodoList.Status, Is.EqualTo(todoList.Status));
            Assert.That(copiedTodoList.Entries.Count, Is.EqualTo(todoList.Entries.Count));

            for (int i = 0; i < copiedTodoList.Entries.Count; i++)
            {
                Assert.That(copiedTodoList.Entries[i].Title, Is.EqualTo(todoList.Entries[i].Title));
                Assert.That(copiedTodoList.Entries[i].Status, Is.EqualTo(todoList.Entries[i].Status));
            }
        }

        [TearDown]
        public void Cleanup()
        {
            this.appDbContext.Database.EnsureDeleted();
        }
    }
}