﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoDomainEntities.Infrastructure;

namespace ToDoAppTests
{
    [TestFixture]
    public class DueDateAttributeTests
    {
        private IsValidDueDateAttribute attribute;

        [SetUp]
        public void SetUp()
        {
            this.attribute = new IsValidDueDateAttribute();
        }

        [Test]
        public void IsValid_ReturnsFalse_ForPastDueDate()
        {
            // Arrange
            var dueDate = DateTime.Now.Date.AddDays(-1);

            // Act
            var result = this.attribute.IsValid(dueDate);

            // Assert
            Assert.That(result, Is.False);
        }

        [Test]
        public void IsValid_ReturnsTrue_ForCurrentDate()
        {
            // Arrange
            var dueDate = DateTime.Now.Date;

            // Act
            var result = this.attribute.IsValid(dueDate);

            // Assert
            Assert.That(result, Is.True);
        }

        [Test]
        public void IsValid_ReturnsTrue_ForFutureDueDate()
        {
            // Arrange
            var dueDate = DateTime.Now.Date.AddDays(1);

            // Act
            var result = this.attribute.IsValid(dueDate);

            // Assert
            Assert.That(result, Is.True);
        }

        [Test]
        public void IsValid_ReturnsFalse_ForNullDueDate()
        {
            // Act
            var result = this.attribute.IsValid(null);

            // Assert
            Assert.That(result, Is.False);
        }
    }

}
