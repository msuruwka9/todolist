﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoDomainEntities.Enums;
using TodoDomainEntities.Infrastructure;

namespace ToDoAppTests
{
    public class EnumExtensionsTests
    {
        [Test]
        public void GetDisplayName_ReturnsCorrectName()
        {
            // Arrange
            var entryStatus = EntryStatus.InProgress;

            // Act
            var displayName = entryStatus.GetDisplayName();

            // Assert
            Assert.That(displayName, Is.EqualTo("In progress"));
        }

        [Test]
        public void GetDisplayName_ReturnsNullForNullValue()
        {
            // Arrange
            EntryStatus? nullValue = null;

            Assert.Throws<NullReferenceException>(() => nullValue.GetDisplayName());
        }

        [Test]
        public void GetDisplayName_ReturnsNullForEnumValueWithoutDisplayAttribute()
        {
            // Arrange
            var valueWithoutAttribute = EnumWithoutDisplayAttribute.SomeValue;

            // Act
            var displayName = valueWithoutAttribute.GetDisplayName();

            // Assert
            Assert.That(displayName, Is.Null);
        }

        private enum EnumWithoutDisplayAttribute
        {
            SomeValue,
        }

    }
}
